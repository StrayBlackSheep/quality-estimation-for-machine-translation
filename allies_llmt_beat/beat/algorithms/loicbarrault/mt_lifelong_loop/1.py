#!/usr/bin/env python
# vim: set fileencoding=utf-8 :

###################################################################################
#                                                                                 #
# Copyright (c) 2019 Idiap Research Institute, http://www.idiap.ch/               #
# Contact: beat.support@idiap.ch                                                  #
#                                                                                 #
# Redistribution and use in source and binary forms, with or without              #
# modification, are permitted provided that the following conditions are met:     #
#                                                                                 #
# 1. Redistributions of source code must retain the above copyright notice, this  #
# list of conditions and the following disclaimer.                                #
#                                                                                 #
# 2. Redistributions in binary form must reproduce the above copyright notice,    #
# this list of conditions and the following disclaimer in the documentation       #
# and/or other materials provided with the distribution.                          #
#                                                                                 #
# 3. Neither the name of the copyright holder nor the names of its contributors   #
# may be used to endorse or promote products derived from this software without   #
# specific prior written permission.                                              #
#                                                                                 #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND #
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   #
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          #
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    #
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      #
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      #
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      #
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   #
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            #
#                                                                                 #
###################################################################################
import numpy as np
import torch
import pickle
import struct
import logging
import random
import platform
import os
import sys
import nmtpytorch
from nmtpytorch.config import Options, TRAIN_DEFAULTS
from nmtpytorch.mainloop import MainLoop
from nmtpytorch.translator import Translator
from nmtpytorch.utils.beat import beat_separate_train_valid
from nmtpytorch.utils.misc import setup_experiment, fix_seed
from nmtpytorch.utils.device import DeviceManager
from nmtpytorch import models
from pathlib import Path
from io import BytesIO
from transquest.algo.transformers.run_model import QuestModel


import six
import copy
import math
import json
from pathlib import Path
from collections import Counter
from collections import defaultdict
from collections import OrderedDict
from functools import partial
from torch import nn
from torchtext.vocab import Vectors
from torch.distributions.normal import Normal
from torch.nn.utils.rnn import pack_padded_sequence as pack
from torch.nn.utils.rnn import pad_packed_sequence as unpack
from torchtext import data

def get_openkiwi():
    # TODO(Config field)****************************************************************************************************


    class EstimatorConfig(object):
        UNK = '<unk>'
        PAD = '<pad>'
        START = '<bos>'
        STOP = '<eos>'
        UNK_ID = 0
        PAD_ID = 1
        START_ID = 2
        STOP_ID = 3
        UNALIGNED_ID = 4
        OK = 'OK'
        BAD = 'BAD'
        OK_ID = 0
        BAD_ID = 1
        LABELS = [OK, BAD]
        SOURCE = 'source'
        TARGET = 'target'
        PE = 'pe'
        TARGET_TAGS = 'tags'
        SOURCE_TAGS = 'source_tags'
        GAP_TAGS = 'gap_tags'
        TAGS = [TARGET_TAGS, SOURCE_TAGS, GAP_TAGS]
        SENTENCE_SCORES = 'sentence_scores'
        BINARY = 'binary'
        MODEL_FILE = 'model.torch'
        VOCAB = 'vocab'
        CONFIG = 'config'
        STATE_DICT = 'state_dict'
        SENT_SIGMA = 'sentence_sigma'
        LOSS = 'loss'
        PREQEFV = 'PreQEFV'
        POSTQEFV = 'PostQEFV'

        def __init__(
                self,
                vocabs,
                hidden_est=100,
                rnn_layers_est=1,
                mlp_est=True,
                dropout_est=0.0,
                start_stop=False,
                predict_target=True,
                predict_gaps=False,
                predict_source=False,
                token_level=True,
                sentence_level=True,
                sentence_ll=True,
                binary_level=True,
                target_bad_weight=2.0,
                source_bad_weight=2.0,
                gaps_bad_weight=2.0,
                hidden_pred=400,
                rnn_layers_pred=3,
                dropout_pred=0.0,
                share_embeddings=False,
                embedding_sizes=0,
                target_embeddings_size=200,
                source_embeddings_size=200,
                out_embeddings_size=200,
                predict_inverse=False,
        ):
            """ Predictor Estimator Hyperparams.
            """
            self.source_vocab_size = len(vocabs['source'])
            self.target_vocab_size = len(vocabs['target'])

            self.start_stop = start_stop or predict_gaps
            self.hidden_est = hidden_est
            self.rnn_layers_est = rnn_layers_est
            self.mlp_est = mlp_est
            self.dropout_est = dropout_est
            self.predict_target = predict_target
            self.predict_gaps = predict_gaps
            self.predict_source = predict_source
            self.token_level = token_level
            self.sentence_level = sentence_level
            self.sentence_ll = sentence_ll
            self.binary_level = binary_level
            self.target_bad_weight = target_bad_weight
            self.source_bad_weight = source_bad_weight
            self.gaps_bad_weight = gaps_bad_weight

            # Vocabulary
            self.target_side = 'target'
            self.source_side = 'source'
            self.predict_inverse = predict_inverse
            if self.predict_inverse:
                self.source_side, self.target_side = (
                    self.target_side,
                    self.source_side,
                )
                self.target_vocab_size, self.source_vocab_size = (
                    self.source_vocab_size,
                    self.target_vocab_size,
                )

            # Architecture
            self.hidden_pred = hidden_pred
            self.rnn_layers_pred = rnn_layers_pred
            self.dropout_pred = dropout_pred
            self.share_embeddings = share_embeddings
            if embedding_sizes:
                self.target_embeddings_size = embedding_sizes
                self.source_embeddings_size = embedding_sizes
                self.out_embeddings_size = embedding_sizes
            else:
                self.target_embeddings_size = target_embeddings_size
                self.source_embeddings_size = source_embeddings_size
                self.out_embeddings_size = out_embeddings_size

        @classmethod
        def from_dict(cls, config_dict, vocabs):
            """ Create config from a saved state_dict.
            """
            config = cls(vocabs)
            config.update(config_dict)
            return config

        def update(self, other_config):
            """Updates the config object with the values of `other_config`
               Args:
                 other_config: The `dict` or `ModelConfig` object to update with.
            """
            config_dict = dict()
            if isinstance(self, other_config.__class__):
                config_dict = other_config.__dict__
            elif isinstance(other_config, dict):
                config_dict = other_config
            self.__dict__.update(config_dict)

        def state_dict(self):
            """Return the __dict__ for serialization.
            """
            self.__dict__['__version__'] = '0.1.3'
            return self.__dict__


    # TODO(Layer field)*****************************************************************************************************


    class MLPScorer(nn.Module):
        """ Implements a score function based on a Multilayer Perceptron.
        """

        def __init__(self, query_size, key_size, layers=2, nonlinearity=nn.Tanh):
            super().__init__()
            layer_list = []
            size = query_size + key_size
            for i in range(layers):
                size_next = size // 2 if i < layers - 1 else 1
                layer_list.append(
                    nn.Sequential(nn.Linear(size, size_next), nonlinearity())
                )
                size = size_next
            self.layers: nn.ModuleList = nn.ModuleList(layer_list)

        def forward(self, query, keys):
            layer_in = torch.cat([query.unsqueeze(1).expand_as(keys), keys], dim=-1)
            layer_in = layer_in.reshape(-1, layer_in.size(-1))
            for layer in self.layers:
                layer_in = layer(layer_in)
            out = layer_in.reshape(keys.size()[:-1])
            return out


    class Attention(nn.Module):
        """ Generic Attention Implementation.
        """

        def __init__(self, scorer):
            super().__init__()
            self.scorer = scorer
            self.mask = None

        def forward(self, query, keys, values=None):
            if values is None:
                values = keys
            scores = self.scorer(query, keys)
            # Masked Softmax
            scores = scores - scores.mean(1, keepdim=True)  # numerical stability
            scores = torch.exp(scores)
            if self.mask is not None:
                scores = self.mask * scores
            convex = scores / scores.sum(1, keepdim=True)
            return torch.einsum('bs,bsi->bi', [convex, values])

        def set_mask(self, mask):
            self.mask = mask


    # TODO(utils field)*****************************************************************************************************


    def apply_packed_sequence(rnn, embedding, lengths):
        """ Runs a forward pass of embeddings through an rnn using packed sequence.
        """
        # Sort Batch by sequence length
        lengths_sorted, permutation = torch.sort(lengths, descending=True)
        embedding_sorted = embedding[permutation]

        # Use Packed Sequence
        embedding_packed = pack(embedding_sorted, lengths_sorted, batch_first=True)
        outputs_packed, (hidden, cell) = rnn(embedding_packed)
        outputs_sorted, _ = unpack(outputs_packed, batch_first=True)
        # Restore original order
        _, permutation_rev = torch.sort(permutation, descending=False)
        outputs = outputs_sorted[permutation_rev]
        hidden, cell = hidden[:, permutation_rev], cell[:, permutation_rev]
        return outputs, (hidden, cell)


    def deserialize_vocabs(vocabs):
        """Restore defaultdict lost in serialization.
        """
        vocabs = dict(vocabs)
        for name, vocab in vocabs.items():
            vocab.stoi = defaultdict(lambda: EstimatorConfig.UNK_ID, vocab.stoi)
        return vocabs


    def fields_from_vocabs(fields, vocabs):
        """ Load Field objects from vocabs dict.
        """
        vocabs = deserialize_vocabs(vocabs)
        for name, vocab in vocabs.items():
            if name not in fields:
                pass
            else:
                fields[name].vocab = vocab
        return fields


    def deserialize_fields_from_vocabs(fields, vocabs):
        """
        Load serialized vocabularies into their fields.
        """
        # TODO redundant deserialization
        vocabs = deserialize_vocabs(vocabs)
        return fields_from_vocabs(fields, vocabs)


    def load_vocabularies_to_fields(vocab_path, fields):
        """Load serialized Vocabularies from disk into fields."""
        if Path(vocab_path).exists():
            vocabs_dict = torch.load(
                str(vocab_path), map_location=lambda storage, loc: storage
            )
            vocabs = vocabs_dict[EstimatorConfig.VOCAB]
            fields = deserialize_fields_from_vocabs(fields, vocabs)
            return all(
                [vocab_loaded_if_needed(field) for _, field in fields.items()]
            )
        return False


    def load_vocabularies_to_datasets(vocab_path, *datasets):
        fields = {}
        for dataset in datasets:
            fields.update(dataset.fields)
        return load_vocabularies_to_fields(vocab_path, fields)


    def vocab_loaded_if_needed(field):
        return not field.use_vocab or (hasattr(field, EstimatorConfig.VOCAB) and field.vocab)


    def build_vocabulary(fields_vocab_options, *datasets):
        fields = {}
        for dataset in datasets:
            fields.update(dataset.fields)

        for name, field in fields.items():
            if not vocab_loaded_if_needed(field):
                kwargs_vocab = fields_vocab_options[name]
                if 'vectors_fn' in kwargs_vocab:
                    vectors_fn = kwargs_vocab['vectors_fn']
                    kwargs_vocab['vectors'] = vectors_fn()
                    del kwargs_vocab['vectors_fn']
                field.build_vocab(*datasets, **kwargs_vocab)
                

    def map_to_polyglot(token):
        mapping = {EstimatorConfig.UNK: '<UNK>',
                   EstimatorConfig.PAD: '<PAD>',
                   EstimatorConfig.START: '<S>',
                   EstimatorConfig.STOP: '</S>'}
        if token in mapping:
            return mapping[token]
        return token


    # TODO(Data field)******************************************************************************************************


    class WordEmbeddings(Vectors):
        def __init__(
            self,
            name,
            emb_format='polyglot',
            binary=True,
            map_fn=lambda x: x,
            **kwargs
        ):
            self.binary = binary
            self.emb_format = emb_format

            self.itos = None
            self.stoi = None
            self.dim = None
            self.vectors = None

            self.map_fn = map_fn
            super().__init__(name, **kwargs)

        def __getitem__(self, token):
            if token in self.stoi:
                token = self.map_fn(token)
                return self.vectors[self.stoi[token]]
            else:
                return self.unk_init(torch.tensor(1, self.dim))

        def cache(self, name, cache, url=None, max_vectors=None):
            if self.emb_format in ['polyglot', 'glove']:
                try:
                    from polyglot.mapping import Embedding
                except ImportError:
                    print('Please install `polyglot` package first.')
                    return None
                if self.emb_format == 'polyglot':
                    embeddings = Embedding.load(name)
                else:
                    embeddings = Embedding.from_glove(name)
                self.itos = embeddings.vocabulary.id_word
                self.stoi = embeddings.vocabulary.word_id
                self.dim = embeddings.shape[1]
                self.vectors = torch.tensor(embeddings.vectors).view(-1, self.dim)

            elif self.emb_format in ['word2vec', 'fasttext']:
                try:
                    from gensim.models import KeyedVectors
                except ImportError:
                    print('Please install `gensim` package first.')
                    return None
                embeddings = KeyedVectors.load_word2vec_format(
                    name, unicode_errors='ignore', binary=self.binary
                )
                self.itos = embeddings.index2word
                self.stoi = dict(zip(self.itos, range(len(self.itos))))
                self.dim = embeddings.vector_size
                self.vectors = torch.tensor(embeddings.vectors).view(-1, self.dim)

            elif self.emb_format == 'text':
                tokens = []
                vectors = []
                if self.binary:
                    import pickle

                    # vectors should be a dict mapping str keys to numpy arrays
                    with open(name, 'rb') as f:
                        d = pickle.load(f)
                        tokens = list(d.keys())
                        vectors = list(d.values())
                else:
                    # each line should contain a token and its following fields
                    # <token> <vector_value_1> ... <vector_value_n>
                    with open(name, 'r', encoding='utf8') as f:
                        for line in f:
                            if line:  # ignore empty lines
                                fields = line.rstrip().split()
                                tokens.append(fields[0])
                                vectors.append(list(map(float, fields[1:])))
                self.itos = tokens
                self.stoi = dict(zip(self.itos, range(len(self.itos))))
                self.vectors = torch.tensor(vectors)
                self.dim = self.vectors.shape[1]


    class SequenceLabelsField(data.Field):
        """Sequence of Labels.
        """

        def __init__(self, classes, *args, **kwargs):
            self.classes = classes
            self.vocab = None
            super().__init__(*args, **kwargs)

        def build_vocab(self, *args, **kwargs):
            specials = self.classes + [
                self.pad_token,
                self.init_token,
                self.eos_token,
            ]
            self.vocab = self.vocab_cls(Counter(), specials=specials, **kwargs)


    class Fieldset:
        ALL = 'all'
        TRAIN = 'train'
        VALID = 'valid'
        TEST = 'test'

        def __init__(self):
            self._fields = {}
            self._options = {}
            self._required = {}
            self._vocab_options = {}
            self._vocab_vectors = {}
            self._file_reader = {}

        def add(
                self,
                name,
                field,
                file_option_suffix,
                required=ALL,
                vocab_options=None,
                vocab_vectors=None,
                file_reader=None,
        ):
            self._fields[name] = field
            self._options[name] = file_option_suffix
            if not isinstance(required, list):
                required = [required]
            self._required[name] = required
            self._file_reader[name] = file_reader

            if vocab_options is None:
                vocab_options = {}
            self._vocab_options[name] = vocab_options
            self._vocab_vectors[name] = vocab_vectors

        @property
        def fields(self):
            return self._fields

        def is_required(self, name, set_name):
            required = self._required[name]
            if set_name in required or self.ALL in required:
                return True
            else:
                return False

        def fields_and_texts(self, set_name, **texts_options):
            """ {'test_source': ['content'], 'test_target': ['content']} """
            fields = {}
            texts = {}
            for name, file_option_suffix in self._options.items():
                text_option = '{}{}'.format(set_name, file_option_suffix)
                text_content = texts_options.get(text_option)
                if not text_content and self.is_required(name, set_name):
                    raise FileNotFoundError(
                        'File {} is required (use the {} '
                        'option).'.format(text_content, text_option.replace('_', '-'))
                    )
                elif text_content:
                    texts[name] = {
                        'name': text_content,
                    }
                    fields[name] = self._fields[name]
            return fields, texts

        def vocab_kwargs(self, name, **kwargs):
            if name not in self._vocab_options:
                raise KeyError(
                    'Field named "{}" does not exist in this fieldset'.format(name)
                )
            vkwargs = {}
            for argument, option_name in self._vocab_options[name].items():
                option_value = kwargs.get(option_name)
                if option_value is not None:
                    vkwargs[argument] = option_value
            return vkwargs

        def vocab_vectors_loader(
            self,
            name,
            embeddings_binary=False,
            **kwargs
        ):
            if name not in self._vocab_vectors:
                raise KeyError(
                    'Field named "{}" does not exist in this fieldset'.format(name)
                )

            def no_vectors_fn():
                return None

            vectors_fn = no_vectors_fn
            option_name = self._vocab_vectors[name]
            if option_name:
                option_value = kwargs.get(option_name)
                if option_value:
                    emb_model = partial(
                        WordEmbeddings, emb_format='polyglot', map_fn=map_to_polyglot
                    )
                    vectors_fn = partial(
                        emb_model, option_value, binary=embeddings_binary
                    )
            return vectors_fn

        def fields_vocab_options(self, **kwargs):
            vocab_options = {}
            for name, field in self.fields.items():
                vocab_options[name] = dict(
                    vectors_fn=self.vocab_vectors_loader(name, **kwargs)
                )
                vocab_options[name].update(self.vocab_kwargs(name, **kwargs))
            return vocab_options


    class Example(object):
        """ Defines a single training or test example.
        """

        @classmethod
        def fromJSON(cls, data, fields):
            return cls.fromdict(json.loads(data), fields)

        @classmethod
        def fromdict(cls, data, fields):
            ex = cls()
            for key, vals in fields.items():
                if key not in data:
                    raise ValueError("Specified key {} was not found in "
                                     "the input data".format(key))
                if vals is not None:
                    if not isinstance(vals, list):
                        vals = [vals]
                    for val in vals:
                        name, field = val
                        setattr(ex, name, field.preprocess(data[key]))
            return ex

        @classmethod
        def fromCSV(cls, data, fields, field_to_index=None):
            if field_to_index is None:
                return cls.fromlist(data, fields)
            else:
                assert(isinstance(fields, dict))
                data_dict = {f: data[idx] for f, idx in field_to_index.items()}
                return cls.fromdict(data_dict, fields)

        @classmethod
        def fromlist(cls, data, fields):
            ex = cls()
            for (name, field), val in zip(fields, data):
                if field is not None:
                    if isinstance(val, six.string_types):
                        val = val.rstrip('\n')
                    # Handle field tuples
                    if isinstance(name, tuple):
                        for n, f in zip(name, field):
                            setattr(ex, n, f.preprocess(val))
                    else:
                        setattr(ex, name, field.preprocess(val))
            return ex

        @classmethod
        def fromtree(cls, data, fields, subtrees=False):
            try:
                from nltk.tree import Tree
            except ImportError:
                print("Please install NLTK. "
                      "See the docs at http://nltk.org for more information.")
                raise
            tree = Tree.fromstring(data)
            if subtrees:
                return [cls.fromlist(
                    [' '.join(t.leaves()), t.label()], fields) for t in tree.subtrees()]
            return cls.fromlist([' '.join(tree.leaves()), tree.label()], fields)


    class Corpus(object):

        def __init__(self, fields_examples=None, dataset_fields=None):
            """ Create a Corpus by specifying examples and fields.
            """
            self.fields_examples = (
                fields_examples if fields_examples is not None else []
            )
            self.dataset_fields = (
                dataset_fields if dataset_fields is not None else []
            )
            self.number_of_examples = (
                len(self.fields_examples[0]) if self.fields_examples else 0
            )

        @classmethod
        def from_texts(cls, fields, texts):
            """Create a QualityEstimationDataset given paths and fields.
            """
            fields_examples = []
            dataset_fields = []
            # first load the data for each field
            for attrib_name, field in fields.items():
                text_dict = texts[attrib_name]
                fields_values_for_example = text_dict['name']
                fields_examples.append(fields_values_for_example)
                dataset_fields.append((attrib_name, field))

            # then add each corresponding sentence from each field
            nb_lines = [len(fe) for fe in fields_examples]
            assert min(nb_lines) == max(nb_lines)  # Assert files have the same size
            return cls(fields_examples, dataset_fields)

        def __iter__(self):
            for j in range(self.number_of_examples):
                fields_values_for_example = [
                    self.fields_examples[i][j]
                    for i in range(len(self.dataset_fields))
                ]
                yield data.Example.fromlist(
                    fields_values_for_example, self.dataset_fields
                )


    class QEDataset(data.Dataset):
        """ Defines a dataset for quality estimation. Based on the WMT 201X. """

        @staticmethod
        def sort_key(ex):
            # don't work for pack_padded_sequences
            # return data.interleave_keys(len(ex.source), len(ex.target))
            return len(ex.source)

        def __init__(self, examples, fields, filter_pred=None):
            """ Create a dataset from a list of Examples and Fields.
            """
            # ensure that examples is not a generator
            examples = list(examples)
            super().__init__(examples, fields, filter_pred)

        def __getstate__(self):
            """For pickling. Copied from OpenNMT-py DatasetBase implementation.
            """
            return self.__dict__

        def __setstate__(self, _d):
            """For pickling. Copied from OpenNMT-py DatasetBase implementation.
            """
            self.__dict__.update(_d)

        def __reduce_ex__(self, proto):
            """For pickling. Copied from OpenNMT-py DatasetBase implementation.
            """
            return super(QEDataset, self).__reduce_ex__(proto)

        def split(
            self,
            split_ratio=0.7,
            stratified=False,
            strata_field='label',
            random_state=None,
        ):
            datasets = super().split(
                split_ratio, stratified, strata_field, random_state
            )
            casted_datasets = [
                QEDataset(examples=dataset.examples, fields=dataset.fields)
                for dataset in datasets
            ]
            return casted_datasets


    def build_text_field():
        return data.Field(
            tokenize=lambda sentence: sentence.strip().split(),
            init_token=EstimatorConfig.START,
            batch_first=True,
            eos_token=EstimatorConfig.STOP,
            pad_token=EstimatorConfig.PAD,
            unk_token=EstimatorConfig.UNK,
        )


    def build_label_field(postprocessing=None):
        return SequenceLabelsField(
            classes=EstimatorConfig.LABELS,
            tokenize=lambda sentence: sentence.strip().split(),
            pad_token=EstimatorConfig.PAD,
            batch_first=True,
            postprocessing=postprocessing,
        )


    def build_fieldset(wmt18_format=False):
        target_field = build_text_field()
        source_field = build_text_field()

        source_vocab_options = dict(
            min_freq='source_vocab_min_frequency', max_size='source_vocab_size'
        )
        target_vocab_options = dict(
            min_freq='target_vocab_min_frequency', max_size='target_vocab_size'
        )

        fieldset = Fieldset()
        fieldset.add(
            name=EstimatorConfig.SOURCE,
            field=source_field,
            file_option_suffix='_source',
            required=Fieldset.TRAIN,
            vocab_options=source_vocab_options,
        )
        fieldset.add(
            name=EstimatorConfig.TARGET,
            field=target_field,
            file_option_suffix='_target',
            required=Fieldset.TRAIN,
            vocab_options=target_vocab_options,
        )
        fieldset.add(
            name=EstimatorConfig.PE,
            field=target_field,
            file_option_suffix='_pe',
            required='',
            vocab_options=target_vocab_options,
        )

        post_pipe_target = data.Pipeline(lambda batch: batch)
        if wmt18_format:
            post_pipe_gaps = data.Pipeline(lambda batch: batch[::2])
            post_pipe_target = data.Pipeline(lambda batch: batch[1::2])
            fieldset.add(
                name=EstimatorConfig.GAP_TAGS,
                field=build_label_field(post_pipe_gaps),
                file_option_suffix='_target_tags',
                required='',
            )

        fieldset.add(
            name=EstimatorConfig.TARGET_TAGS,
            field=build_label_field(post_pipe_target),
            file_option_suffix='_target_tags',
            required='',
        )
        fieldset.add(
            name=EstimatorConfig.SOURCE_TAGS,
            field=build_label_field(),
            file_option_suffix='_source_tags',
            required='',
        )
        fieldset.add(
            name=EstimatorConfig.SENTENCE_SCORES,
            field=data.Field(
                sequential=False, use_vocab=False, dtype=torch.float32
            ),
            file_option_suffix='_sentence_scores',
            required='',
        )

        pipe = data.Pipeline(lambda x: math.ceil(float(x)))
        fieldset.add(
            name=EstimatorConfig.BINARY,
            field=data.Field(
                sequential=False,
                use_vocab=False,
                dtype=torch.long,
                preprocessing=pipe,
            ),
            file_option_suffix='_sentence_scores',
            required='',
        )
        return fieldset


    def build_dataset(fieldset, prefix, **kwargs):
        fields, texts = fieldset.fields_and_texts(prefix, **kwargs)
        examples = Corpus.from_texts(fields=fields, texts=texts)
        dataset = QEDataset(
            examples=examples, fields=fields
        )
        return dataset


    def build_test_dataset(fieldset, load_vocab=None, **kwargs):
        """ Build a test QE dataset.
        """
        test_dataset = build_dataset(fieldset, prefix=Fieldset.TEST, **kwargs)

        fields_vocab_options = fieldset.fields_vocab_options(**kwargs)
        if load_vocab:
            vocab_path = Path(load_vocab)
            load_vocabularies_to_datasets(vocab_path, test_dataset)
        else:
            build_vocabulary(fields_vocab_options, test_dataset)

        return test_dataset


    def build_bucket_iterator(dataset, device, batch_size, is_train):
        device_obj = None if device is None else torch.device(device)
        iterator = data.BucketIterator(
            dataset=dataset,
            batch_size=batch_size,
            repeat=False,
            sort_key=dataset.sort_key,
            sort=False,
            # sorts the data within each minibatch in decreasing order
            # set to true if you want use pack_padded_sequences
            sort_within_batch=is_train,
            # shuffle batches
            shuffle=is_train,
            device=device_obj,
            train=is_train,
        )
        return iterator


    # TODO(Model field)*****************************************************************************************************


    class Model(nn.Module):

        def __init__(self, vocabs, ConfigCls=None, config=None, **kwargs):
            """ Quality Estimation Base Class.
            """
            super().__init__()

            self.vocabs = vocabs

            if config is None:
                config = ConfigCls(vocabs=vocabs, **kwargs)
            else:
                config = ConfigCls.from_dict(config_dict=config, vocabs=vocabs)
                assert not kwargs
            self.config = config

        def num_parameters(self):
            return sum(p.numel() for p in self.parameters())

        def predict(self, batch, class_name=EstimatorConfig.BAD, unmask=True):
            model_out = self(batch)
            predictions = {}
            class_index = torch.tensor([EstimatorConfig.LABELS.index(class_name)])

            for key in model_out:
                if key in [EstimatorConfig.TARGET_TAGS, EstimatorConfig.SOURCE_TAGS, EstimatorConfig.GAP_TAGS]:
                    # Models are assumed to return logits, not probabilities
                    logits = model_out[key]
                    probs = torch.softmax(logits, dim=-1)
                    class_probs = probs.index_select(
                        -1, class_index.to(device=probs.device)
                    )
                    class_probs = class_probs.squeeze(-1).tolist()
                    if unmask:
                        if key == EstimatorConfig.SOURCE_TAGS:
                            input_key = EstimatorConfig.SOURCE
                        else:
                            input_key = EstimatorConfig.TARGET
                        mask = self.get_mask(batch, input_key)
                        if key == EstimatorConfig.GAP_TAGS:
                            # Append one extra token
                            mask = torch.cat(
                                [mask.new_ones((mask.shape[0], 1)), mask], dim=1
                            )

                        lengths = mask.int().sum(dim=-1)
                        for i, x in enumerate(class_probs):
                            class_probs[i] = x[: lengths[i]]
                    predictions[key] = class_probs
                elif key == EstimatorConfig.SENTENCE_SCORES:
                    predictions[key] = model_out[key].tolist()
                elif key == EstimatorConfig.BINARY:
                    logits = model_out[key]
                    probs = torch.softmax(logits, dim=-1)
                    class_probs = probs.index_select(
                        -1, class_index.to(device=probs.device)
                    )
                    predictions[key] = class_probs.tolist()

            return predictions

        def predict_raw(self, examples):
            batch = self.preprocess(examples)
            return self.predict(batch, class_name=EstimatorConfig.BAD, unmask=True)

        def get_mask(self, batch, output):
            """ Compute Mask of Tokens for side.
            """
            side = output
            input_tensor = getattr(batch, side)
            if isinstance(input_tensor, tuple) and len(input_tensor) == 2:
                input_tensor, lengths = input_tensor

            mask = torch.ones_like(input_tensor, dtype=torch.uint8)

            possible_padding = [EstimatorConfig.PAD, EstimatorConfig.START, EstimatorConfig.STOP]
            unk_id = self.vocabs[side].stoi.get(EstimatorConfig.UNK)
            for pad in possible_padding:
                pad_id = self.vocabs[side].stoi.get(pad)
                if pad_id is not None and pad_id != unk_id:
                    mask &= torch.as_tensor(
                        input_tensor != pad_id,
                        device=mask.device,
                        dtype=torch.uint8,
                    )

            return mask

        @staticmethod
        def load_torch_file(file_path):
            file_path = Path(file_path)
            if not file_path.exists():
                raise FileNotFoundError('Torch file not found: {}'.format(file_path))

            file_dict = torch.load(
                str(file_path), map_location=lambda storage, loc: storage
            )
            if isinstance(file_dict, Path):
                # Resolve cases where file is just a link to another torch file
                linked_path = file_dict
                if not linked_path.exists():
                    relative_path = (
                            file_path.with_name(file_dict.name) / EstimatorConfig.MODEL_FILE
                    )
                    if relative_path.exists():
                        linked_path = relative_path
                return Model.load_torch_file(linked_path)
            return file_dict

        @classmethod
        def from_file(cls, path):
            model_dict = torch.load(
                str(path), map_location=lambda storage, loc: storage
            )
            if cls.__name__ not in model_dict:
                raise KeyError(
                    '{} model data not found in {}'.format(cls.__name__, path)
                )

            return cls.from_dict(model_dict)

        @classmethod
        def from_dict(cls, model_dict):
            vocabs = deserialize_vocabs(model_dict[EstimatorConfig.VOCAB])
            class_dict = model_dict[cls.__name__]
            model = cls(vocabs=vocabs, config=class_dict[EstimatorConfig.CONFIG])
            model.load_state_dict(class_dict[EstimatorConfig.STATE_DICT], False)
            return model

        def save(self, path):
            vocabs = self.serialize_vocabs(self.vocabs)
            model_dict = {
                '__version__': '0.1.3',
                EstimatorConfig.VOCAB: vocabs,
                self.__class__.__name__: {
                    EstimatorConfig.CONFIG: self.config.state_dict(),
                    EstimatorConfig.STATE_DICT: self.state_dict(),
                },
            }
            torch.save(model_dict, str(path))


    class Predictor(Model):
        """ Bidirectional Conditional Language Model
        """

        def __init__(self, vocabs, **kwargs):
            super().__init__(vocabs=vocabs, ConfigCls=EstimatorConfig, **kwargs)

            scorer = MLPScorer(
                self.config.hidden_pred * 2, self.config.hidden_pred * 2, layers=2
            )

            self.attention = Attention(scorer)
            self.embedding_source = nn.Embedding(
                self.config.source_vocab_size,
                self.config.source_embeddings_size,
                EstimatorConfig.PAD_ID,
            )
            self.embedding_target = nn.Embedding(
                self.config.target_vocab_size,
                self.config.target_embeddings_size,
                EstimatorConfig.PAD_ID,
            )
            self.lstm_source = nn.LSTM(
                input_size=self.config.source_embeddings_size,
                hidden_size=self.config.hidden_pred,
                num_layers=self.config.rnn_layers_pred,
                batch_first=True,
                dropout=self.config.dropout_pred,
                bidirectional=True,
            )
            self.forward_target = nn.LSTM(
                input_size=self.config.target_embeddings_size,
                hidden_size=self.config.hidden_pred,
                num_layers=self.config.rnn_layers_pred,
                batch_first=True,
                dropout=self.config.dropout_pred,
                bidirectional=False,
            )
            self.backward_target = nn.LSTM(
                input_size=self.config.target_embeddings_size,
                hidden_size=self.config.hidden_pred,
                num_layers=self.config.rnn_layers_pred,
                batch_first=True,
                dropout=self.config.dropout_pred,
                bidirectional=False,
            )

            self.W1 = self.embedding_target
            if not self.config.share_embeddings:
                self.W1 = nn.Embedding(
                    self.config.target_vocab_size,
                    self.config.out_embeddings_size,
                    EstimatorConfig.PAD_ID,
                )
            self.W2 = nn.Parameter(
                torch.zeros(
                    self.config.out_embeddings_size, self.config.out_embeddings_size
                )
            )
            self.V = nn.Parameter(
                torch.zeros(
                    2 * self.config.target_embeddings_size,
                    2 * self.config.out_embeddings_size,
                )
            )
            self.C = nn.Parameter(
                torch.zeros(
                    2 * self.config.hidden_pred, 2 * self.config.out_embeddings_size
                )
            )
            self.S = nn.Parameter(
                torch.zeros(
                    2 * self.config.hidden_pred, 2 * self.config.out_embeddings_size
                )
            )
            for p in self.parameters():
                if len(p.shape) > 1:
                    nn.init.xavier_uniform_(p)

        @staticmethod
        def from_options(vocabs, opts):
            model = Predictor(
                vocabs,
                hidden_pred=opts.hidden_pred,
                rnn_layers_pred=opts.rnn_layers_pred,
                dropout_pred=opts.dropout_pred,
                share_embeddings=opts.share_embeddings,
                embedding_sizes=opts.embedding_sizes,
                target_embeddings_size=opts.target_embeddings_size,
                source_embeddings_size=opts.source_embeddings_size,
                out_embeddings_size=opts.out_embeddings_size,
                predict_inverse=opts.predict_inverse,
            )
            return model

        def forward(self, batch, source_side=None, target_side=None):
            if not source_side:
                source_side = self.config.source_side
            if not target_side:
                target_side = self.config.target_side

            source = getattr(batch, source_side)
            target = getattr(batch, target_side)
            batch_size, target_len = target.shape[:2]
            # Remove First and Last Element (Start / Stop Tokens)
            source_mask = self.get_mask(batch, source_side)[:, 1:-1]
            source_lengths = source_mask.sum(1)
            target_lengths = self.get_mask(batch, target_side).sum(1)
            source_embeddings = self.embedding_source(source)
            target_embeddings = self.embedding_target(target)
            # Source Encoding
            source_contexts, hidden = apply_packed_sequence(
                self.lstm_source, source_embeddings, source_lengths
            )
            # Target Encoding.
            h_forward, h_backward = self._split_hidden(hidden)
            forward_contexts, _ = self.forward_target(target_embeddings, h_forward)
            target_emb_rev = self._reverse_padded_seq(
                target_lengths, target_embeddings
            )
            backward_contexts, _ = self.backward_target(target_emb_rev, h_backward)
            backward_contexts = self._reverse_padded_seq(
                target_lengths, backward_contexts
            )

            # For each position, concatenate left context i-1 and right context i+1
            target_contexts = torch.cat(
                [forward_contexts[:, :-2], backward_contexts[:, 2:]], dim=-1
            )
            # For each position i, concatenate Emeddings i-1 and i+1
            target_embeddings = torch.cat(
                [target_embeddings[:, :-2], target_embeddings[:, 2:]], dim=-1
            )

            # Get Attention vectors for all positions and stack.
            self.attention.set_mask(source_mask.float())
            attns = [
                self.attention(
                    target_contexts[:, i], source_contexts, source_contexts
                )
                for i in range(target_len - 2)
            ]
            attns = torch.stack(attns, dim=1)

            # Combine attention, embeddings and target context vectors
            C = torch.einsum('bsi,il->bsl', [attns, self.C])
            V = torch.einsum('bsj,jl->bsl', [target_embeddings, self.V])
            S = torch.einsum('bsk,kl->bsl', [target_contexts, self.S])
            t_tilde = C + V + S
            # Maxout with pooling size 2
            t, _ = torch.max(
                t_tilde.view(
                    t_tilde.shape[0], t_tilde.shape[1], t_tilde.shape[-1] // 2, 2
                ),
                dim=-1,
            )

            f = torch.einsum('oh,bso->bsh', [self.W2, t])
            logits = torch.einsum('vh,bsh->bsv', [self.W1.weight, f])
            PreQEFV = torch.einsum('bsh,bsh->bsh', [self.W1(target[:, 1:-1]), f])
            PostQEFV = torch.cat([forward_contexts, backward_contexts], dim=-1)
            return {
                target_side: logits,
                EstimatorConfig.PREQEFV: PreQEFV,
                EstimatorConfig.POSTQEFV: PostQEFV,
            }

        @staticmethod
        def _reverse_padded_seq(lengths, sequence):
            """ Reverses a batch of padded sequences of different length.
            """
            batch_size, max_length = sequence.shape[:-1]
            reversed_idx = []
            for i in range(batch_size * max_length):
                batch_id = i // max_length
                sent_id = i % max_length
                if sent_id < lengths[batch_id]:
                    sent_id_rev = lengths[batch_id] - sent_id - 1
                else:
                    sent_id_rev = sent_id  # Padding symbol, don't change order
                reversed_idx.append(max_length * batch_id + sent_id_rev)
            flat_sequence = sequence.contiguous().view(batch_size * max_length, -1)
            reversed_seq = flat_sequence[reversed_idx, :].view(*sequence.shape)
            return reversed_seq

        @staticmethod
        def _split_hidden(hidden):
            """ Split Hidden State into forward/backward parts.
            """
            h, c = hidden
            size = h.shape[0]
            idx_forward = torch.arange(0, size, 2, dtype=torch.long)
            idx_backward = torch.arange(1, size, 2, dtype=torch.long)
            hidden_forward = (h[idx_forward], c[idx_forward])
            hidden_backward = (h[idx_backward], c[idx_backward])
            return hidden_forward, hidden_backward


    class Estimator(Model):

        def __init__(
                self, vocabs, predictor_tgt=None, predictor_src=None, **kwargs
        ):
            super().__init__(vocabs=vocabs, ConfigCls=EstimatorConfig, **kwargs)

            if predictor_src:
                self.config.update(predictor_src.config)
            elif predictor_tgt:
                self.config.update(predictor_tgt.config)

            # Predictor Settings #
            predict_tgt = (
                    self.config.predict_target
                    or self.config.predict_gaps
                    or self.config.sentence_level
            )
            if predict_tgt and not predictor_tgt:
                predictor_tgt = Predictor(
                    vocabs=vocabs,
                    predict_inverse=False,
                    hidden_pred=self.config.hidden_pred,
                    rnn_layers_pred=self.config.rnn_layers_pred,
                    dropout_pred=self.config.dropout_pred,
                    target_embeddings_size=self.config.target_embeddings_size,
                    source_embeddings_size=self.config.source_embeddings_size,
                    out_embeddings_size=self.config.out_embeddings_size,
                )
            if self.config.predict_source and not predictor_src:
                predictor_src = Predictor(
                    vocabs=vocabs,
                    predict_inverse=True,
                    hidden_pred=self.config.hidden_pred,
                    rnn_layers_pred=self.config.rnn_layers_pred,
                    dropout_pred=self.config.dropout_pred,
                    target_embeddings_size=self.config.target_embeddings_size,
                    source_embeddings_size=self.config.source_embeddings_size,
                    out_embeddings_size=self.config.out_embeddings_size,
                )

            # Update the predictor vocabs if token level == True
            # Required by `get_mask` call in predictor forward with `pe` side
            # to determine padding IDs.
            if self.config.token_level:
                if predictor_src:
                    predictor_src.vocabs = vocabs
                if predictor_tgt:
                    predictor_tgt.vocabs = vocabs

            self.predictor_tgt = predictor_tgt
            self.predictor_src = predictor_src

            predictor_hidden = self.config.hidden_pred
            embedding_size = self.config.out_embeddings_size
            input_size = 2 * predictor_hidden + embedding_size

            self.nb_classes = len(EstimatorConfig.LABELS)
            self.lstm_input_size = input_size

            self.mlp = None
            self.sentence_pred = None
            self.sentence_sigma = None
            self.binary_pred = None
            self.binary_scale = None

            # Build Model #

            if self.config.start_stop:
                self.start_PreQEFV = nn.Parameter(torch.zeros(1, 1, embedding_size))
                self.end_PreQEFV = nn.Parameter(torch.zeros(1, 1, embedding_size))

            if self.config.mlp_est:
                self.mlp = nn.Sequential(
                    nn.Linear(input_size, self.config.hidden_est), nn.Tanh()
                )
                self.lstm_input_size = self.config.hidden_est

            self.lstm = nn.LSTM(
                input_size=self.lstm_input_size,
                hidden_size=self.config.hidden_est,
                num_layers=self.config.rnn_layers_est,
                batch_first=True,
                dropout=self.config.dropout_est,
                bidirectional=True,
            )
            self.embedding_out = nn.Linear(
                2 * self.config.hidden_est, self.nb_classes
            )
            if self.config.predict_gaps:
                self.embedding_out_gaps = nn.Linear(
                    4 * self.config.hidden_est, self.nb_classes
                )
            self.dropout = None
            if self.config.dropout_est:
                self.dropout = nn.Dropout(self.config.dropout_est)

            # Multitask Learning Objectives #
            sentence_input_size = (
                    2 * self.config.rnn_layers_est * self.config.hidden_est
            )
            if self.config.sentence_level:
                self.sentence_pred = nn.Sequential(
                    nn.Linear(sentence_input_size, sentence_input_size // 2),
                    nn.Sigmoid(),
                    nn.Linear(sentence_input_size // 2, sentence_input_size // 4),
                    nn.Sigmoid(),
                    nn.Linear(sentence_input_size // 4, 1),
                )
                self.sentence_sigma = None
                if self.config.sentence_ll:
                    # Predict truncated Gaussian distribution
                    self.sentence_sigma = nn.Sequential(
                        nn.Linear(sentence_input_size, sentence_input_size // 2),
                        nn.Sigmoid(),
                        nn.Linear(
                            sentence_input_size // 2, sentence_input_size // 4
                        ),
                        nn.Sigmoid(),
                        nn.Linear(sentence_input_size // 4, 1),
                        nn.Sigmoid(),
                    )
            if self.config.binary_level:
                self.binary_pred = nn.Sequential(
                    nn.Linear(sentence_input_size, sentence_input_size // 2),
                    nn.Tanh(),
                    nn.Linear(sentence_input_size // 2, sentence_input_size // 4),
                    nn.Tanh(),
                    nn.Linear(sentence_input_size // 4, 2),
                )
            # self.xents: nn.ModuleDict = nn.ModuleDict()
            # weight = make_loss_weights(
            #     self.nb_classes, EstimatorConfig.BAD_ID, self.config.target_bad_weight
            # )
            #
            # self.xents[EstimatorConfig.TARGET_TAGS] = nn.CrossEntropyLoss(
            #     reduction='sum', ignore_index=EstimatorConfig.PAD_TAGS_ID, weight=weight
            # )
            # if self.config.predict_source:
            #     weight = make_loss_weights(
            #         self.nb_classes, EstimatorConfig.BAD_ID, self.config.source_bad_weight
            #     )
            #     self.xents[EstimatorConfig.SOURCE_TAGS] = nn.CrossEntropyLoss(
            #         reduction='sum', ignore_index=EstimatorConfig.PAD_TAGS_ID, weight=weight
            #     )
            # if self.config.predict_gaps:
            #     weight = make_loss_weights(
            #         self.nb_classes, EstimatorConfig.BAD_ID, self.config.gaps_bad_weight
            #     )
            #     self.xents[EstimatorConfig.GAP_TAGS] = nn.CrossEntropyLoss(
            #         reduction='sum', ignore_index=EstimatorConfig.PAD_TAGS_ID, weight=weight
            #     )
            # if self.config.sentence_level and not self.config.sentence_ll:
            #     self.mse_loss = nn.MSELoss(reduction='sum')
            # if self.config.binary_level:
            #     self.xent_binary = nn.CrossEntropyLoss(reduction='sum')

        @staticmethod
        def from_options(vocabs, opts):
            predictor_src = predictor_tgt = None
            if opts.load_pred_source:
                predictor_src = Predictor.from_file(opts.load_pred_source)
            if opts.load_pred_target:
                predictor_tgt = Predictor.from_file(opts.load_pred_target)

            model = Estimator(
                vocabs,
                predictor_tgt=predictor_tgt,
                predictor_src=predictor_src,
                hidden_est=opts.hidden_est,
                rnn_layers_est=opts.rnn_layers_est,
                mlp_est=opts.mlp_est,
                dropout_est=opts.dropout_est,
                start_stop=opts.start_stop,
                predict_target=opts.predict_target,
                predict_gaps=opts.predict_gaps,
                predict_source=opts.predict_source,
                token_level=opts.token_level,
                sentence_level=opts.sentence_level,
                sentence_ll=opts.sentence_ll,
                binary_level=opts.binary_level,
                target_bad_weight=opts.target_bad_weight,
                source_bad_weight=opts.source_bad_weight,
                gaps_bad_weight=opts.gaps_bad_weight,
                hidden_pred=opts.hidden_pred,
                rnn_layers_pred=opts.rnn_layers_pred,
                dropout_pred=opts.dropout_pred,
                share_embeddings=opts.dropout_est,
                embedding_sizes=opts.embedding_sizes,
                target_embeddings_size=opts.target_embeddings_size,
                source_embeddings_size=opts.source_embeddings_size,
                out_embeddings_size=opts.out_embeddings_size,
                predict_inverse=opts.predict_inverse,
            )
            return model

        def forward(self, batch):
            outputs = OrderedDict()
            contexts_tgt, h_tgt = None, None
            contexts_src, h_src = None, None
            # 预测目标标签、目标空格、句子水平
            if (
                    self.config.predict_target
                    or self.config.predict_gaps
                    or self.config.sentence_level
            ):
                # 构建Predictor模型输出得到Estimator模型的输入
                model_out_tgt = self.predictor_tgt(batch)
                input_seq, target_lengths = self.make_input(
                    model_out_tgt, batch, EstimatorConfig.TARGET_TAGS
                )
                # 执行双向lstm模型获取estimator模型的特征层
                contexts_tgt, h_tgt = apply_packed_sequence(
                    self.lstm, input_seq, target_lengths
                )
                # 预测目标标签
                if self.config.predict_target:
                    logits = self.predict_tags(contexts_tgt)
                    if self.config.start_stop:
                        logits = logits[:, 1:-1]
                    outputs[EstimatorConfig.TARGET_TAGS] = logits
                # 预测目标空白
                if self.config.predict_gaps:
                    contexts_gaps = self.make_contexts_gaps(contexts_tgt)
                    logits = self.predict_tags(
                        contexts_gaps, out_embed=self.embedding_out_gaps
                    )
                    outputs[EstimatorConfig.GAP_TAGS] = logits
            # 预测原语言标签
            if self.config.predict_source:
                model_out_src = self.predictor_src(batch)
                input_seq, target_lengths = self.make_input(
                    model_out_src, batch, EstimatorConfig.SOURCE_TAGS
                )
                contexts_src, h_src = apply_packed_sequence(
                    self.lstm, input_seq, target_lengths
                )

                logits = self.predict_tags(contexts_src)
                outputs[EstimatorConfig.SOURCE_TAGS] = logits

            # 构建句子水平的输入
            sentence_input = self.make_sentence_input(h_tgt, h_src)
            # 预测句子水平的结果
            if self.config.sentence_level:
                outputs.update(self.predict_sentence(sentence_input))
            # 预测句子二分类水平的结果
            if self.config.binary_level:
                bin_logits = self.binary_pred(sentence_input).squeeze()
                outputs[EstimatorConfig.BINARY] = bin_logits
            # 预测token水平的原到pe或pe到目标的结果
            if self.config.token_level and hasattr(batch, EstimatorConfig.PE):
                if self.predictor_tgt:
                    model_out = self.predictor_tgt(batch, target_side=EstimatorConfig.PE)
                    logits = model_out[EstimatorConfig.PE]
                    outputs[EstimatorConfig.PE] = logits
                if self.predictor_src:
                    model_out = self.predictor_src(batch, source_side=EstimatorConfig.PE)
                    logits = model_out[EstimatorConfig.SOURCE]
                    outputs[EstimatorConfig.SOURCE] = logits

            return outputs

        def make_input(self, model_out, batch, tagset):
            """Make Input Sequence from predictor outputs. """
            PreQEFV = model_out[EstimatorConfig.PREQEFV]
            PostQEFV = model_out[EstimatorConfig.POSTQEFV]
            side = EstimatorConfig.TARGET
            if tagset == EstimatorConfig.SOURCE_TAGS:
                side = EstimatorConfig.SOURCE
            token_mask = self.get_mask(batch, side)
            batch_size = token_mask.shape[0]
            target_lengths = token_mask.sum(1)
            if self.config.start_stop:
                target_lengths += 2
                start = self.start_PreQEFV.expand(
                    batch_size, 1, self.config.out_embeddings_size
                )
                end = self.end_PreQEFV.expand(
                    batch_size, 1, self.config.out_embeddings_size
                )
                PreQEFV = torch.cat((start, PreQEFV, end), dim=1)
            else:
                PostQEFV = PostQEFV[:, 1:-1]

            input_seq = torch.cat([PreQEFV, PostQEFV], dim=-1)
            length, input_dim = input_seq.shape[1:]
            if self.mlp:
                input_flat = input_seq.view(batch_size * length, input_dim)
                input_flat = self.mlp(input_flat)
                input_seq = input_flat.view(
                    batch_size, length, self.lstm_input_size
                )
            return input_seq, target_lengths

        def make_contexts_gaps(self, contexts):
            # Concat Contexts Shifted
            contexts = torch.cat((contexts[:, :-1], contexts[:, 1:]), dim=-1)
            return contexts

        def make_sentence_input(self, h_tgt, h_src):
            """Reshape last hidden state. """
            h = h_tgt[0] if h_tgt else h_src[0]
            h = h.contiguous().transpose(0, 1)
            return h.reshape(h.shape[0], -1)

        def predict_sentence(self, sentence_input):
            """Compute Sentence Score predictions."""
            outputs = OrderedDict()
            sentence_scores = self.sentence_pred(sentence_input).squeeze()
            outputs[EstimatorConfig.SENTENCE_SCORES] = sentence_scores
            if self.sentence_sigma:
                # Predict truncated Gaussian on [0,1]
                sigma = self.sentence_sigma(sentence_input).squeeze()
                outputs[EstimatorConfig.SENT_SIGMA] = sigma
                outputs['SENT_MU'] = outputs[EstimatorConfig.SENTENCE_SCORES]
                mean = outputs['SENT_MU'].clone().detach()
                # Compute log-likelihood of x given mu, sigma
                normal = Normal(mean, sigma)
                # Renormalize on [0,1] for truncated Gaussian
                partition_function = (normal.cdf(1) - normal.cdf(0)).detach()
                outputs[EstimatorConfig.SENTENCE_SCORES] = mean + (
                        (
                                sigma ** 2
                                * (normal.log_prob(0).exp() - normal.log_prob(1).exp())
                        )
                        / partition_function
                )

            return outputs

        def predict_tags(self, contexts, out_embed=None):
            """Compute Tag Predictions."""
            if not out_embed:
                out_embed = self.embedding_out
            batch_size, length, hidden = contexts.shape
            if self.dropout:
                contexts = self.dropout(contexts)
            # Fold sequence length in batch dimension
            contexts_flat = contexts.contiguous().view(-1, hidden)
            logits_flat = out_embed(contexts_flat)
            logits = logits_flat.view(batch_size, length, self.nb_classes)
            return logits


    class Predicter(object):

        def __init__(self, model, fields=None):
            """ Class to load a model for inference.
            """

            self.model = model
            self.fields = fields
            # Will break in Multi GPU mode
            self._device = next(model.parameters()).device

        def to(self, device):
            """ Method to mode Predicter object to other device. e.g: "cuda"
            """

            self._device = device
            self.model.to(device)

        def predict(self, examples, batch_size=1):
            """ Create Predictions for a list of examples.
            """
            if not examples:
                return defaultdict(list)
            if self.fields is None:
                raise Exception('Missing fields object.')

            if not examples.get(EstimatorConfig.SOURCE):
                raise KeyError('Missing required field "{}"'.format(EstimatorConfig.SOURCE))
            if not examples.get(EstimatorConfig.TARGET):
                raise KeyError('Missing required field "{}"'.format(EstimatorConfig.TARGET))

            if not all(
                    [s.strip() for s in examples[EstimatorConfig.SOURCE] + examples[EstimatorConfig.TARGET]]
            ):
                raise Exception(
                    'Empty String in {} or {} field found!'.format(
                        EstimatorConfig.SOURCE, EstimatorConfig.TARGET
                    )
                )
            fields = [(name, self.fields[name]) for name in examples]

            field_examples = [
                Example.fromlist(values, fields)
                for values in zip(*examples.values())
            ]

            dataset = QEDataset(field_examples, fields=fields)

            return self.run(dataset, batch_size)

        def run(self, dataset, batch_size=1):
            iterator = build_bucket_iterator(
                dataset, self._device, batch_size, is_train=False
            )
            self.model.eval()
            predictions = defaultdict(list)
            with torch.no_grad():
                for batch in iterator:
                    model_pred = self.model.predict(batch)
                    for key, values in model_pred.items():
                        if isinstance(values, list):
                            predictions[key] += values
                        else:
                            predictions[key].append(values)
            return dict(predictions)
    
    return Estimator, build_test_dataset, build_fieldset, Predicter
 

model_type = 'kiwi'  # choose in ['trans', 'kiwi']
if model_type == 'kiwi':
    Estimator, build_test_dataset, build_fieldset, Predicter = get_openkiwi()
    model_path = r'/root/allies_llmt_beat/beat/openkiwi/runs/estimator/latest_epoch/model.torch'
    model_dict = Estimator.load_torch_file(model_path)
    model: Estimator = Estimator.from_dict(model_dict)
elif model_type == 'trans':
    model_path = r'/root/allies_llmt_beat/beat/openkiwi/runs/transquest/TransQuest-HTER-EN-DE-NMT'
    model = QuestModel('xlmroberta', model_path, num_labels=1, use_cuda=torch.cuda.is_available())
else:
    raise ValueError("model_type for now only support ['trans', 'kiwi']")
    
 
def quality_estimation_for_openkiwi(request, source, target):
    gpu_id = 0
    if torch.cuda.is_available() and gpu_id >= 0:
            model.to(gpu_id)
    test_dataset = build_test_dataset(
        fieldset=build_fieldset(),
        load_vocab=model_path,
        **{'test_source': [source], 'test_target': [target]},
    )
    predictions = Predicter(model).run(test_dataset, 1)
    print(request, predictions)


def quality_estimation_for_transquest(request, source, target):
    test_sentence_pairs = [[source, target]]
    predictions, _ = model.predict(test_sentence_pairs)
    print(request, {'sentence_score': [predictions.tolist()]})


def generate_system_request_to_user(model, file_id, source, current_hypothesis):
    """
    Include here your code to ask "questions" to the human in the loop

    Prototype of this function can be adapted to your need as it is internal to this block
    but it must return a request as a dictionary object as shown below

    request_type can only be "reference" for now
        if "reference", the question asked to the user is: What is the reference translation for sentence sentence_id of the document file_id
                the cost of this question is proportional to the length of the sentence to translate

    file_id must be an existing file_id of a document in the lifelong corpus
    sentence_id must be a unsigned int between 0 and document length
    """
    # choose a sentence to be translated by the human translator
    # For now, we'll use a random sentence
    # TODO: find a better way to decide which sentence to translate
    sid = random.randint(0, len(source) - 1)

    # beat_logger.debug("### mt_lifelong_loop:generate_system_request_to_user")
    request = {
        "request_type": "reference",
        "file_id": '{}'.format(file_id),
        "sentence_id": np.uint32(sid)
    }
    #print('@@@@', source[np.uint32(sid)].replace('@@ ', ''))
    #print('@@@@', current_hypothesis[np.uint32(sid)].replace('@@ ', ''))
    source_sentence = source[np.uint32(sid)].replace('@@ ', '')
    target_sentence = current_hypothesis[np.uint32(sid)].replace('@@ ', '')
    if model_type == 'trans':
        quality_estimation_for_transquest(request, source_sentence, target_sentence)
    elif model_type == 'kiwi':
        quality_estimation_for_openkiwi(request, source_sentence, target_sentence)
    return request
# generate_system_request_to_user(None, 1, [12, 12], None)   


beat_logger = logging.getLogger('beat_lifelong_mt')
beat_logger.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG)

TRANSLATE_DEFAULTS = {
    'suppress_unk': False,  # Do not generate <unk> tokens.')
    'disable_filters': True,  # Disable eval_filters given in config')
    'n_best': False,  # Generate n-best list of beam candidates.')
    'batch_size': 16,  # Batch size for beam-search')
    'beam_size': 6,  # Beam size for beam-search')
    'max_len': 200,  # Maximum sequence length to produce (Default: 200)')
    'lp_alpha': 0.,  # Apply length-penalty (Default: 0.)')
    'device-id': 'gpu',  # cpu or gpu (Default: gpu)')
    'models': [],  # Saved model/checkpoint file(s)")
    'task_id': 'src:Text -> trg:Text',  # Task to perform, e.g. en_src:Text, pt_src:Text -> fr_tgt:Text')
    'override': [],  # (section).key:value overrides for config")
    'stochastic': False,  # Don't fix seed for sampling-based models.")
    'beam_func': 'beam_search',  # Use a custom beam search method defined in the model.")
    # You can translate a set of splits defined in the .conf file with -s
    # Or you can provide another input configuration with -S
    # With this, the previous -s is solely used for the naming of the output file
    # and you can only give 1 split with -s that corresponds to new input you define with -S.
    'splits': 'lifelong',  # Comma separated splits from config file')
    'source': None,  # Comma-separated key:value pairs to provide new inputs.')
    'output': '/not/used/because/beat_platform'  # Output filename prefix')
}
'''
TRANSLATE_DEFAULTS = {}
'''


def mt_to_allies(hypothesis):
    hyps = {"text": hypothesis}
    return hyps


def run_translation(translator, source, file_id):
    """
    """
    beat_logger.debug("### mt_lifelong_loop:run_translation for {}".format(file_id))
    ####################################################################################
    #  Forward pass on the model
    ####################################################################################
    # TODO: prepare lifelong data to translate
    translator.update_split_source(TRANSLATE_DEFAULTS['splits'], source)

    # Run translation on the new input using mt baseline system
    hypothesis = translator()

    return hypothesis


def unsupervised_model_adaptation(source,
                                  file_id,
                                  data_dict,
                                  model,
                                  translator,
                                  current_hypothesis):
    # beat_logger.debug("### mt_lifelong_loop:unsupervised_model_adaptation")

    ########################################################
    # ACCESS TRAINING DATA TO ADAPT IF WANTED
    ########################################################
    # You are allowed to re-process training data all along the lifelong adaptation.
    # The code below shows how to acces data from the training set.

    # The training and valid data is accessible through the data_dict
    # See below how to get all training data
    # The function beat_separate_train_valid allows to split the data into train/valid as during training (with loss of document information)
    data_dict_train, data_dict_valid = beat_separate_train_valid(data_dict)

    # Get the list of training files
    # We create a dictionnary with file_id as keys and the index of the file in the training set as value.
    # This dictionnary will be used later to access the training files by file_id

    ########################################################
    # Shows how to access training data per INDEX
    #
    # Shows how to access source  and file_info from
    # a training file by using its index in the training set
    ########################################################
    file_index = 2  # index of the file we want to access
    # file_id_per_index = train_loader[file_index][0]['train_file_info'].file_id
    # time_stamp_per_index = train_loader[file_index][0]['train_file_info'].time_stamp
    # supervision_per_index = train_loader[file_index][0]['train_file_info'].supervision
    # source_per_index = train_loader[file_index][0]["train_source"].value

    ########################################################
    # Shows how to access training data per file_id
    #
    # Shows how to access source text and file_info from
    # a training file by using its file_id
    ########################################################
    # Assume that we know the file_id, note that we stored them earlier in a dictionnary
    train_file_id = list(data_dict.keys())[2]
    time_stamp_per_ID = data_dict[train_file_id]["file_info"]['time_stamp']
    supervision_per_ID = data_dict[train_file_id]["file_info"]['supervision']
    source_per_ID = data_dict[train_file_id]["source"]

    # TODO: Update the model and the translator object

    return model, translator


def prepare_model_for_tuning(model_data, params, data_dict_tune):
    # set the params similarly to train_model
    # add the pretrained options

    params['pretrained_file'] = model_data
    # params['eval_zero'] = True
    params['pretrained_layers'] = ''  # comma sep. list of layer prefixes to initialize
    params['freeze_layers'] = ''  # comma sep. list of layer prefixes to freeze

    # Prepare tuning and valid data
    params['data'] = {}
    params['data']['train_set'] = {}
    params['data']['train_set']['src'] = data_dict_tune['src']
    params['data']['train_set']['trg'] = data_dict_tune['trg']
    # NOTE: No need to valid for finetuning, simply run several epochs
    # self.params['data']['val_set']={}
    # self.params['data']['val_set']['src']=self.valid_data['src']
    # self.params['data']['val_set']['trg']=valid_data['trg']

    # get the vocabularies from the model (need to load it with torch then check data['opts']['vocabulary']['src'] / ['trg']
    in_stream = BytesIO(model_data)
    model = torch.load(in_stream)

    params['vocabulary'] = {}
    params['vocabulary']['src'] = model['opts']['vocabulary']['src']
    params['vocabulary']['trg'] = model['opts']['vocabulary']['trg']
    params['filename'] = '/not/needed/beat_platform'
    params['sections'] = ['train', 'model', 'data', 'vocabulary']

    opts = Options.from_dict(params, {})

    setup_experiment(opts, beat_platform=True)
    dev_mgr = DeviceManager("gpu")

    # If given, seed that; if not generate a random seed and print it
    if opts.train['seed'] > 0:
        seed = fix_seed(opts.train['seed'])
    else:
        opts.train['seed'] = fix_seed()

    # Instantiate the model object
    adapt_model = getattr(models, opts.train['model_type'])(opts=opts, beat_platform=True)

    beat_logger.info("Python {} -- torch {} with CUDA {} (on machine '{}')".format(
        platform.python_version(), torch.__version__,
        torch.version.cuda, platform.node()))
    beat_logger.info("nmtpytorch {}".format(nmtpytorch.__version__))
    beat_logger.info(dev_mgr)
    beat_logger.info("Seed for further reproducibility: {}".format(opts.train['seed']))

    loop = MainLoop(adapt_model, opts.train, dev_mgr, beat_platform=True)

    return loop


def select_data(file_id, doc_source, data_dict_train, sent_id, user_answer):
    # Let's simply select closest document for now

    # For now, let's CHEAT and get the document
    data_dict_tune = {}
    data_dict_tune['src'] = []
    data_dict_tune['trg'] = []

    al_src = doc_source[sent_id]
    al_trg = user_answer.answer

    # FIXME: for now, just tune on the current sentence (debugging)
    # for sent in data_dict_train['src']:
    #    data_dict_tune['src'].append(sent)
    data_dict_tune['src'].append(al_src)
    # for sent in data_dict_train['trg']:
    #    data_dict_tune['trg'].append(sent)
    data_dict_tune['trg'].append(al_trg)

    return data_dict_tune


def online_adaptation(model, params, data_dict_train, file_id, doc_source, current_hypothesis, sent_id, user_answer):
    """
    Include here your code to adapt the model according
    to the human domain expert answer to the request

    Prototype of this function can be adapted to your need as it is internatl to this block
    """
    beat_logger.debug("### mt_lifelong_loop:online_adaptation")
    # print("### mt_lifelong_loop:online_adaptation")

    # Tune the model with data similar to the document we want to translate
    data_dict_tune = select_data(file_id, doc_source, data_dict_train, sent_id, user_answer)

    # prepare model for adaptation
    loop = prepare_model_for_tuning(model, params, data_dict_tune)
    # Run the adaptation
    new_model = loop()
    # NOTE: for debugging, just pass the previous model
    # new_model= model

    return new_model


class Algorithm:
    """
    Main class of the mt_lifelong_loop
    """

    def __init__(self):
        beat_logger.debug("### mt_lifelong_loop:init")
        self.model = None
        self.adapted_model = None
        self.translator = None
        self.data_dict_train = None
        self.data_dict_dev = None
        self.translate_params = TRANSLATE_DEFAULTS
        self.init_end_index = -1

    def setup(self, parameters):

        self.params = {}
        self.params['train'] = TRAIN_DEFAULTS
        self.params['model'] = {}

        self.params['train']['seed'] = int(parameters['seed'])
        self.params['train']['model_type'] = parameters['model_type']
        self.params['train']['patience'] = int(parameters['patience'])
        self.params['train']['max_epochs'] = 2  # int(parameters['max_epochs'])
        # self.params['train']['eval_freq']=int(parameters['eval_freq'])
        # NOTE: Disable validation during finetuning
        self.params['train']['eval_freq'] = -1
        # self.params['train']['eval_metrics']=parameters['eval_metrics']
        # self.params['train']['eval_metrics']=None
        # self.params['train']['eval_filters']=parameters['eval_filters']
        # self.params['train']['eval_filters']=None
        # self.params['train']['eval_beam']=int(parameters['eval_beam'])
        # self.params['train']['eval_beam']=None
        # self.params['train']['eval_batch_size']=int(parameters['eval_batch_size'])
        # self.params['train']['eval_batch_size']=None
        self.params['train']['save_best_metrics'] = parameters['save_best_metrics']
        self.params['train']['eval_max_len'] = int(parameters['eval_max_len'])
        self.params['train']['checkpoint_freq'] = int(parameters['checkpoint_freq'])
        # self.params['train']['n_checkpoints']=parameters['n_checkpoints']
        self.params['train']['l2_reg'] = int(parameters['l2_reg'])
        self.params['train']['lr_decay'] = parameters['lr_decay']
        self.params['train']['lr_decay_revert'] = parameters['lr_decay_revert']
        self.params['train']['lr_decay_factor'] = parameters['lr_decay_factor']
        self.params['train']['lr_decay_patience'] = int(parameters['lr_decay_patience'])
        self.params['train']['gclip'] = int(parameters['gclip'])
        self.params['train']['optimizer'] = parameters['optimizer']
        self.params['train']['lr'] = parameters['lr']
        self.params['train']['batch_size'] = int(parameters['batch_size'])
        self.params['train']['save_optim_state'] = False

        self.params['train']['save_path'] = Path("/not/used/because/beat_platform")
        # self.params['train']['tensorboard_dir']="/lium/users/barrault/llmt/tensorboard"

        self.params['model']['att_type'] = parameters['att_type']
        self.params['model']['att_bottleneck'] = parameters['att_bottleneck']
        self.params['model']['enc_dim'] = int(parameters['enc_dim'])
        self.params['model']['dec_dim'] = int(parameters['dec_dim'])
        self.params['model']['emb_dim'] = int(parameters['emb_dim'])
        self.params['model']['dropout_emb'] = parameters['dropout_emb']
        self.params['model']['dropout_ctx'] = parameters['dropout_ctx']
        self.params['model']['dropout_out'] = parameters['dropout_out']
        self.params['model']['n_encoders'] = int(parameters['n_encoders'])
        self.params['model']['tied_emb'] = parameters['tied_emb']
        self.params['model']['dec_init'] = parameters['dec_init']
        self.params['model']['bucket_by'] = "src"
        if parameters['max_len'] == "None":
            self.params['model']['max_len'] = None
        else:
            self.params['model']['max_len'] = int(parameters['max_len'])
        self.params['model']['direction'] = "src:Text -> trg:Text"

        return True

    def process(self, inputs, data_loaders, outputs, loop_channel):
        # print("### mt_lifelong_loop:process start -- that's great!")
        """

        """

        ########################################################
        # RECEIVE INCOMING INFORMATION FROM THE FILE TO PROCESS
        ########################################################

        # Access source text of the current file to process
        source = inputs["processor_lifelong_source"].data.text

        if self.translator is None or self.data_dict_train is None:
            # Get the model after initial training
            dl = data_loaders[0]
            (data, _, end_index) = dl[0]

            # Create the nmtpy Translator object to translate
            if self.translator is None:
                model_data = data['model'].value
                self.model = struct.pack('{}B'.format(len(model_data)), *list(model_data))
                # Create a Translator object from nmtpy
                self.translate_params['models'] = [self.model]
                self.translate_params['source'] = source
                self.translator = Translator(beat_platform=True, **self.translate_params)

            if self.data_dict_train is None:
                data_dict = pickle.loads(data["processor_train_data"].text.encode("latin1"))
                self.data_dict_train, self.data_dict_dev = beat_separate_train_valid(data_dict)

        # Access incoming file information
        # See documentation for a detailed description of the mt_file_info
        file_info = inputs["processor_lifelong_file_info"].data
        file_id = file_info.file_id
        supervision = file_info.supervision
        time_stamp = file_info.time_stamp

        beat_logger.debug(
            "mt_lifelong_loop::process: received document {} ({} sentences) to translate ".format(file_id, len(source)))
        # beat_logger.debug('mt_lifelong_loop::process: source = {}'.format(source))

        # TODO: prepare train/valid data for fine-tuning (eventually) -- might not be needed actually as the data is already contained in the training params -> this can be huge!
        current_hypothesis = run_translation(self.translator, source, file_id)
        # If human assisted learning is ON
        ###################################################################################################
        # Interact with the human if necessary
        # This section exchange information with the user simulation and ends up with a new hypothesis
        ###################################################################################################
        human_assisted_learning = supervision in ["active", "interactive"]

        if not human_assisted_learning:
            # In this method, see how to access initial training data to adapt the model
            # for the new incoming data
            self.model, self.translator = unsupervised_model_adaptation(source,
                                                                        file_id,
                                                                        self.train_data,
                                                                        self.model,
                                                                        self.translator,
                                                                        current_hypothesis
                                                                        )
            # update current_hypothesis with current model
            current_hypothesis = run_translation(self.translator, source, file_id)

        # If human assisted learning mode is on (active or interactive learning)
        while human_assisted_learning:

            # Create an empty request that is used to initiate interactive learning
            # For the case of active learning, this request is overwritten by your system itself
            # For now, only requests of type 'reference' are allowed (i.e. give me the reference translation for sentence 'sentence_id' of file 'file_id')

            if supervision == "active":
                # The system can send a question to the human in the loop
                # by using an object of type request
                # The request is the question asked to the system
                request = generate_system_request_to_user(self.model, file_id, source, current_hypothesis)

                # Send the request to the user and wait for the answer
                message_to_user = {
                    "file_id": file_id,  # ID of the file the question is related to
                    "hypothesis": current_hypothesis[request['sentence_id']],  # The current hypothesis
                    "system_request": request,  # the question for the human in the loop
                }
                # beat_logger.debug("mt_lifelong_loop::process: send message to user: request={}".format(request))
                human_assisted_learning, user_answer = loop_channel.validate(message_to_user)

                # Take into account the user answer to generate a new hypothesis and possibly update the model
                self.adapted_model = online_adaptation(self.model, self.params, self.data_dict_train, file_id, source,
                                                       current_hypothesis, request['sentence_id'], user_answer)

                # Update the translator object with the current model
                model_data = struct.pack('{}B'.format(len(self.adapted_model)), *list(self.adapted_model))
                self.translate_params['models'] = [model_data]
                self.translator = Translator(beat_platform=True, **self.translate_params)

                # Generate a new translation
                new_hypothesis = run_translation(self.translator, source, file_id)
                # NOTE: let's debug by simply using the previous translation
                # new_hypothesis = current_hypothesis

                # beat_logger.debug("BEFORE online_adaptation: {}".format(current_hypothesis))
                # beat_logger.debug("AFTER online_adaptation : {}".format(new_hypothesis))
                # Update the current hypothesis with the new one
                current_hypothesis = new_hypothesis
                human_assisted_learning = False

            else:
                human_assisted_learning = False

        # End of human assisted learning
        # Send the current hypothesis
        #    self.init_end_index = 0
        # beat_logger.debug("HYPOTHESIS: {}".format(current_hypothesis))
        print("mt_lifelong_loop::process: translated document {}: ".format(file_id))
        outputs["hypothesis"].write(mt_to_allies(current_hypothesis))

        if not inputs.hasMoreData():
            pass
        # always return True, it signals BEAT to continue processing
        return True
