do_train = True

if do_train:
    from kiwi import train

    train(r'/root/openkiwi/exper/train_estimator_with_sentence_level.yaml')
else:
    from kiwi import predict

    print(predict(r'/root/openkiwi/exper/predict_predictor_with_sentence_level.yaml'))
