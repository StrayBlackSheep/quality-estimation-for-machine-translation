import kiwi
import torch
from kiwi import constants as const
from kiwi.models.predictor import Predictor
from kiwi.data import utils
from kiwi.predictors.predictor import Predicter


def quality_estimation(text):
    model_dict = torch.load(
                str(r'D:\PROJECTS\openkiwi\demo\runs\predictor\latest_epoch\model.torch'), map_location=lambda storage, loc: storage
            )
    vocabs = utils.deserialize_vocabs(model_dict[const.VOCAB])
    class_dict = model_dict['Predictor']
    model:Predictor = Predictor(vocabs=vocabs, config=class_dict[const.CONFIG])
    model.load_state_dict(class_dict[const.STATE_DICT])
    batch = {'source': [vocabs['source'].stoi[v] for v in text.split()]}
    with torch.no_grad():
        model_pred = model(batch)
        print(model_pred)
quality_estimation('Agenda of the next sitting: see Minutes')
# print(kiwi.predict(r'D:\PROJECTS\openkiwi\exper\predict_estimator_with_sentence_level.yaml'))